# this file would be skipped by gitlab cicd pipeline, it is here for local build
FROM openjdk:8-jdk-alpine
EXPOSE 8080

COPY target/*.jar /app.jar
ENTRYPOINT ["java","-jar","/app.jar"]