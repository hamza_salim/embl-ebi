# EMBL-EBI

Spring boot application

spring boot version - 2.4.2

java version - 8

Database H2(in-memory database)

### Clone EMBL-EBI repositoory from gitlab using below command

```
git clone https://gitlab.com/hamza_salim/embl-ebi.git
```
### containerize and run using docker 

Inside the root directory, run the below command: 

```
mvn clean install (mvn clean install -DskipTests)
docker build  -t embl-ebi  -f Dockerfile .
docker run --rm  -it -p 8080:8080 embl-ebi
```
now you can access app on http://localhost:8080/swagger-ui.html

### How to access all the API's, so for that i configure swagger to access all the API's.
### Order to access all the url's to make sure you will get proper response's
### 1- Create, 2- Get, 3- Update, 4- Get, 5- Delete
  
 ###Sample request json for create and update
 
 ### 1) Create 
 ```
 {"person": [
 {
 "first_name": "John",
 "last_name": "Keynes",
 "age": "29",
 "favourite_colour": "red"
 },
 {
 "first_name": "Sarah",
 "last_name": "Robinson",
 "age": "54",
 "favourite_colour": "blue"
 }
 ]
 }
 ``` 
 
 ### 2) Update
 ```
 {
     "person": [
         {
             "person_id": 1,
             "first_name": "Hamza",
             "last_name": "Salim",
             "age": "29",
             "favourite_colour": "violet"
         }
     ]
 }
 ```
