package com.embl.person.EMBLEBI;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class EmblEbiApplication {

	public static void main(String[] args) {
		log.debug("Starting EMBL-EBI application...");
		SpringApplication.run(EmblEbiApplication.class, args);
	}

}
