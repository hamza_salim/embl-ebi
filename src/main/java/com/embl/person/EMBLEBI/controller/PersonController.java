package com.embl.person.EMBLEBI.controller;

import com.embl.person.EMBLEBI.dto.PersonDTO;
import com.embl.person.EMBLEBI.exception.PersonException;
import com.embl.person.EMBLEBI.service.PersonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/v1/person")
@RequiredArgsConstructor
@Slf4j
public class PersonController {

    private final PersonService personService;

    @GetMapping
    PersonDTO getAllPerson(){
        return personService.getAllPerson();
    }

    @PostMapping
    PersonDTO createPerson(@RequestBody PersonDTO personDTO){
        try {
            return personService.saveOrUpdate(personDTO);
        }catch (Exception e){
            return null;
        }
    }

    @PutMapping
    PersonDTO updatePerson(@RequestBody PersonDTO personDTO){
        try{
            return personService.saveOrUpdate(personDTO);
        }catch (Exception e){
            return null;
        }
    }

    @DeleteMapping("{id}")
    void deletePerson(@PathVariable("id") Long id){
        try{
            personService.deletePeronById(id);
        }catch (RuntimeException e){
            throw new PersonException(e.getMessage());
        }
    }
}
