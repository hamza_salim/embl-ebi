package com.embl.person.EMBLEBI.dto;

import com.embl.person.EMBLEBI.model.Person;
import lombok.Data;

import java.util.List;

@Data
public class PersonDTO {

    List<Person> person;
}
