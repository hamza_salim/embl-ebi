package com.embl.person.EMBLEBI.exception;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ExceptionResponseDTO {

    private String message;
    private LocalDateTime dateTime;


}
