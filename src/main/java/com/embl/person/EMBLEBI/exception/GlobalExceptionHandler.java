package com.embl.person.EMBLEBI.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(PersonException.class)
    public ResponseEntity<Object> exceptionHandler(PersonException exception, WebRequest webRequest){
        ExceptionResponseDTO dto = new ExceptionResponseDTO();
        dto.setMessage(exception.getMessage());
        dto.setDateTime(LocalDateTime.now());
        return new ResponseEntity<>(dto,HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
