package com.embl.person.EMBLEBI.exception;

import java.time.LocalDateTime;

public class PersonException extends RuntimeException {

    private String message;
    private LocalDateTime dateTime;


    public PersonException() {
        super();
    }

    public PersonException(Throwable throwable) {
        super(throwable);
    }

    public PersonException(String msg, Throwable throwable) {
        super(msg, throwable);
    }

    public PersonException(String msg){
        super(msg);
    }

    public PersonException(String message, LocalDateTime dateTime) {
        super();
        this.message = message;
        this.dateTime = dateTime;
    }

}
