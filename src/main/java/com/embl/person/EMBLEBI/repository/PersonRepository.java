package com.embl.person.EMBLEBI.repository;

import com.embl.person.EMBLEBI.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {

}
