package com.embl.person.EMBLEBI.service;

import com.embl.person.EMBLEBI.dto.PersonDTO;
import com.embl.person.EMBLEBI.model.Person;
import com.embl.person.EMBLEBI.repository.PersonRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface PersonService {
    PersonDTO getAllPerson();
    PersonDTO saveOrUpdate(PersonDTO person) ;
    void deletePeronById(Long id) throws RuntimeException;

    @Service
    @AllArgsConstructor
    @Slf4j
    class Impl implements PersonService{

        private final PersonRepository personRepository;

        @Override
        @Transactional(readOnly = true)
        public PersonDTO getAllPerson(){
            log.info("fetching all person details..");
            PersonDTO dto = new PersonDTO();
            dto.setPerson((List<Person>) personRepository.findAll());
            return dto;
        }

        @Transactional
        @Override
        public PersonDTO saveOrUpdate(PersonDTO personDTO) {
            PersonDTO dto = new PersonDTO();
                List<Person> personList = personDTO.getPerson().stream().filter(p -> p.getPerson_id() == null).collect(Collectors.toList());
                if(personList.size()>0){
                    log.info("storing all person details into DB.");
                    personRepository.saveAll(personList);
                }else {
                    log.info("updating all person details into DB.");
                    personDTO.getPerson().stream().filter(p -> p.getPerson_id() != null).forEach(
                            p -> {
                                    Optional<Person> personDetail = personRepository.findById(p.getPerson_id());
                                    if(personDetail.isPresent()){
                                        Person _person = personDetail.get();
                                        _person.setAge(p.getAge());
                                        _person.setFavourite_colour(p.getFavourite_colour());
                                        _person.setFirst_name(p.getFirst_name());
                                        _person.setLast_name(p.getLast_name());
                                        personList.add(personRepository.save(_person));
                                    }
                            });
                }

            dto.setPerson(personList);
            return dto;
        }

        @Transactional
        @Override
        public void deletePeronById(Long id) throws RuntimeException{
            try{
                log.info("delete person details from DB. having id: {}",id);
                personRepository.deleteById(id);
            }catch (RuntimeException e){
                throw new RuntimeException("Error while deleting entity");
            }
        }
    }
}
