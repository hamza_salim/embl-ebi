package com.embl.person.EMBLEBI.controller;

import com.embl.person.EMBLEBI.dto.PersonDTO;
import com.embl.person.EMBLEBI.model.Person;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testCreate() throws Exception{
        PersonDTO dto = getPerson(false);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(dto );
        MvcResult result = mockMvc.perform(post("/v1/person/").contentType("application/json").content(requestJson)).andExpect(status().isOk()).andExpect(content().contentType("application/json")).andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        PersonDTO personDTO = objectMapper.readValue(contentAsString, new TypeReference<PersonDTO>() {});

        assertNotNull("most not be null",personDTO);

    }

    @Test
    public void testUpdate() throws Exception{
        PersonDTO dto = getPerson(true);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(dto );
        MvcResult result = mockMvc.perform(put("/v1/person/").contentType("application/json").content(requestJson)).andExpect(status().isOk()).andExpect(content().contentType("application/json")).andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        PersonDTO personDTO = objectMapper.readValue(contentAsString, new TypeReference<PersonDTO>() {});

        assertNotNull("most not be null",personDTO);
    }

    @Test
    public void testFindAll() throws Exception{
        MvcResult result = mockMvc.perform(get("/v1/person/")).andExpect(status().isOk()).andExpect(content().contentType("application/json")).andReturn();
        String contentAsString = result.getResponse().getContentAsString();
        PersonDTO person = objectMapper.readValue(contentAsString, new TypeReference<PersonDTO>() {});

       // assertNotNull("most not be null",person);
       // assertTrue("size must not be zero",person.getPerson().size()>0);
    }

    @Test
    public void testDelete() throws Exception{
        PersonDTO dto = getPerson(true);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(dto );
        mockMvc.perform(delete("/v1/person/1/")).andExpect(status().isOk()).andReturn();
    }

    private PersonDTO getPerson(boolean flag){
        Person person = new Person();
        if(flag)
            person.setPerson_id(1l);

        person.setAge("31");
        person.setFavourite_colour("blue");
        person.setFirst_name("Rani");
        person.setLast_name("salim");
        List<Person> personList = new ArrayList<>();
        personList.add(person);
        PersonDTO dto = new PersonDTO();
        dto.setPerson(personList);
        return dto;
    }

}
