package com.embl.person.EMBLEBI.service;

import com.embl.person.EMBLEBI.dto.PersonDTO;
import com.embl.person.EMBLEBI.model.Person;
import com.embl.person.EMBLEBI.repository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
public class PersonServiceTest {

    @InjectMocks
    PersonService.Impl personService;

    @Mock
    PersonRepository personRepository;

    @Test
    public void testGetAllPerson(){
        Mockito.when(personRepository.findAll()).thenReturn(getPersonList(false));
        PersonDTO personDTO = personService.getAllPerson();
        assertNotNull(personDTO);
    }

    @Test
    public void testCreate() {
        Mockito.when(personRepository.save(new Person())).thenReturn(new Person());
        personService.saveOrUpdate(getPerson(false));
    }

    @Test
    public void testUpdate() {
        Mockito.when(personRepository.save(new Person())).thenReturn(new Person());
        personService.saveOrUpdate(getPerson(true));
    }

    private PersonDTO getPerson(boolean flag){
        List<Person> personList = getPersonList(flag);
        PersonDTO dto = new PersonDTO();
        dto.setPerson(personList);
        return dto;
    }

    private List<Person> getPersonList(boolean flag){
        Person person = new Person();
        if(flag)
            person.setPerson_id(1L);
        person.setAge("31");
        person.setFavourite_colour("blue");
        person.setFirst_name("Rani");
        person.setLast_name("salim");
        List<Person> personList = new ArrayList<>();
        personList.add(person);
        return personList;
    }
}
